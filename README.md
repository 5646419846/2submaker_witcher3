# 2submaker_witcher3
Program for creating a double subtitles (subtitles in two languages at the same time) for the game Witcher 3.
(Программа для создания двойных субтитров (субтитры на двух языках одновременно) для игры Witcher 3.)
## Compiling
```
fpc 2submaker_witcher3.pas
```
## Usage
```
2submaker_witcher3 source_file.w3strings.csv destination_file.w3strings.csv
```
